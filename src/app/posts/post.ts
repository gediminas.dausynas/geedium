export interface Post {
    title: string;
    posted: number;
    content: string;
}