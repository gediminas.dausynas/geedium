import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './post';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.styl']
})

export class PostsComponent implements OnInit {
  posts: Observable<Post[]>;
  
  constructor(private http: HttpClient) {
    this.getPosts()
  }

  getPosts() {
    try {
      this.posts = this.http.get<Post[]>('https://api.geedium.com/api/articles');
    }
    catch(_e) {
      this.posts = null;
    }
  }

  ngOnInit() {

  }
}